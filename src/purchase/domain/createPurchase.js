const { v4: uuidv4 } = require('uuid');
const createPurchase = require('../service/createPurchase');

const { publishPurchaseCreated } = require('../service/publishPurchaseCreated');
const { PurchaseCreatedEvent } = require('../schema/event/purchaseCreated');

module.exports = async (eventPayload, eventMeta) => {
  const { dni, products } = eventPayload;

  const purchase = {
    id: uuidv4(),
    dni,
    products
  };

  // Service Async
  await Promise.all([
    createPurchase(purchase),
    publishPurchaseCreated(new PurchaseCreatedEvent(purchase, eventMeta))
  ]);

  // Body is always the payload of the command response
  return { body: purchase };
};
