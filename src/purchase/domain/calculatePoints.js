const updatePoints = require('../service/updatePoints');

module.exports = async (eventPayload, eventMeta) => {
  const { dni, products } = eventPayload;

  let points = 0;
  let point;
  products.forEach(product => {
    point = Math.trunc(product.final_price / 200);
    points += point;
  });

  // update user
  await updatePoints(dni, points);
};
