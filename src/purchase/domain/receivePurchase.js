const {
  CreatePurchaseValidation
} = require('../schema/input/createPurchaseValidation');
const { ErrorHandled } = require('ebased/util/error');

const getClientByDni = require('../../client/service/getClientsByDni');

const sendCreatePurchaseQueue = require('../service/sendCreatePurchaseQueue');
const { PurchaseCreateEvent } = require('../schema/event/purchaseCreate');

module.exports = async (commandPayload, commandMeta) => {
  new CreatePurchaseValidation(commandPayload, commandMeta);

  const { dni: dniClient } = commandPayload;

  const { Item } = await getClientByDni(dniClient);
  if (!Item)
    throw new ErrorHandled('The client does not exist', {
      status: 400,
      code: 'code0xx',
      layer: 'domain'
    });

  await sendCreatePurchaseQueue(
    new PurchaseCreateEvent(commandPayload, commandMeta)
  );

  return { status: 200, body: commandPayload };
};
