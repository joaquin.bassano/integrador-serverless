const getClientByDni = require('../../client/service/getClientsByDni');

const updatePurchase = require('../service/updatePurchase');

const sendCalculatePointsQueue = require('../service/sendCalculatePointsQueue');
const { CalculatePointsEvent } = require('../schema/event/calculatePoints');

module.exports = async (eventPayload, eventMeta) => {
  const body = JSON.parse(eventPayload.Message);

  const { id, dni, products } = body;

  const { Item } = await getClientByDni(dni);

  const discount = Item.creditCard.type === 'Classic' ? 8 : 12;

  // poner en un helper
  products.forEach(product => {
    product.final_price =
      product.list_price - (product.list_price * discount) / 100;
  });

  await Promise.all([
    updatePurchase(id, products),
    sendCalculatePointsQueue(new CalculatePointsEvent(body, eventMeta))
  ]);
};
