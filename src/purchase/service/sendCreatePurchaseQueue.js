const config = require('ebased/util/config');
const sqs = require('ebased/service/downstream/sqs');

const CREATE_PURCHASE_QUEUE = config.get('PURCHASE_CREATE_QUEUE');

const sendCreatePurchaseQueue = async PurchaseCreateEvent => {
  const { eventPayload, eventMeta } = PurchaseCreateEvent.get();

  const sqsSendParams = {
    QueueUrl: CREATE_PURCHASE_QUEUE,
    MessageBody: eventPayload
  };
  await sqs.send(sqsSendParams, eventMeta);
};

module.exports = sendCreatePurchaseQueue;
