const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const updatePoints = async (dni, points) => {
  const params = {
    TableName: TABLE_NAME,
    Key: { dni },
    ExpressionAttributeNames: {
      '#P': 'points'
    },
    UpdateExpression: 'set #P = :p',
    ExpressionAttributeValues: {
      ':p': points
    }
  };

  await dynamo.updateItem(params);
};

module.exports = updatePoints;
