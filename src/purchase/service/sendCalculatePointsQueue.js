const config = require('ebased/util/config');
const sqs = require('ebased/service/downstream/sqs');

const CALCULATE_POINTS_QUEUE = config.get('CALCULATE_POINTS_QUEUE');

const sendCalculatePointsQueue = async CalculatePointsEvent => {
  const { eventPayload, eventMeta } = CalculatePointsEvent.get();

  const sqsSendParams = {
    QueueUrl: CALCULATE_POINTS_QUEUE,
    MessageBody: eventPayload
  };
  await sqs.send(sqsSendParams, eventMeta);
};

module.exports = sendCalculatePointsQueue;
