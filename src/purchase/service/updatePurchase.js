const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('PURCHASES_TABLE');

const updatePurchase = async (id, products) => {
  const params = {
    TableName: TABLE_NAME,
    Key: { id },
    ExpressionAttributeNames: {
      '#P': 'products'
    },
    UpdateExpression: 'set #P = :p',
    ExpressionAttributeValues: {
      ':p': products
    }
  };

  await dynamo.updateItem(params);
};

module.exports = updatePurchase;
