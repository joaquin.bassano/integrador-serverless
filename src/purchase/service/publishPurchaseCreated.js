const config = require('ebased/util/config');
const sns = require('ebased/service/downstream/sns');

const PURCHASE_CREATED_TOPIC = config.get('PURCHASE_CREATED_TOPIC');

const publishPurchaseCreated = async purchaseCreatedEvent => {
  const { eventPayload, eventMeta } = purchaseCreatedEvent.get();
  const snsPublishParams = {
    TopicArn: PURCHASE_CREATED_TOPIC,
    Message: eventPayload
  };
  await sns.publish(snsPublishParams, eventMeta);
};

module.exports = { publishPurchaseCreated };
