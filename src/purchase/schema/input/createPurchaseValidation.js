const { InputValidation } = require('ebased/schema/inputValidation');
const Schemy = require('schemy');

const productsSchemy = new Schemy({
  strict: false,
  name: { type: String, required: true },
  list_price: { type: Number, required: true }
});

class CreatePurchaseValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'PURCHASE.CREATE_PURCHASE',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        dni: { type: String, max: 9, required: true },
        products: {
          type: [productsSchemy],
          required: true
        }
      }
    });
  }
}

module.exports = { CreatePurchaseValidation };
