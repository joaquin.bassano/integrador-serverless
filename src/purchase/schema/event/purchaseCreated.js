const { DownstreamEvent } = require('ebased/schema/downstreamEvent');
const Schemy = require('schemy');

const productsSchemy = new Schemy({
  strict: false,
  name: { type: String, required: true },
  list_price: { type: Number, required: true }
});

class PurchaseCreatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'PURCHASE.PURCHASE_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        id: { type: String, required: true },
        dni: { type: String, max: 9, required: true },
        products: {
          type: [productsSchemy],
          required: true
        }
      }
    });
  }
}

module.exports = { PurchaseCreatedEvent };
