const {
  CreateClientValidation
} = require('../schema/input/createClientValidation');

//   const getAge = require('../helper/getAge');
const createGift = require('../helper/createGift');

const updateGift = require('../service/updateGift');

module.exports = async (eventPayload, eventMeta) => {
  const body = JSON.parse(eventPayload.Message);

  new CreateClientValidation(body, eventMeta);

  const { dni, dateOfBirth } = body;

  const gift = createGift(dateOfBirth);

  await updateGift(dni, gift);
};
