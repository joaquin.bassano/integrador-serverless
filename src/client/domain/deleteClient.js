const {
  DeleteClientValidation
} = require('../schema/input/deleteClientValidation');

const { ErrorHandled } = require('ebased/util/error');

const getClientByDni = require('../service/getClientsByDni');

const deleteClient = require('../service/deleteClient');

module.exports = async (commandPayload, commandMeta) => {
  new DeleteClientValidation(commandPayload, commandMeta);
  const { dni } = commandPayload;

  const { Item } = await getClientByDni(dni);
  if (!Item)
    throw new ErrorHandled('The client does not exist', {
      status: 400,
      code: 'code0xx',
      layer: 'domain'
    });

  await deleteClient(dni);

  return { status: 200 };
};
