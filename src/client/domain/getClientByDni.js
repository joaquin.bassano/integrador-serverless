const { ErrorHandled } = require('ebased/util/error');

const getClientByDni = require('../service/getClientsByDni');

const { GetClientValidation } = require('../schema/input/getClientValidation');

module.exports = async (commandPayload, commandMeta) => {
  new GetClientValidation(commandPayload, commandMeta);
  const { dni } = commandPayload;

  //const dniParam =

  const { Item } = await getClientByDni(dni);

  if (!Item)
    throw new ErrorHandled('The client does not exist', {
      status: 400,
      code: 'code0xx',
      layer: 'domain'
    });

  return { status: 200, body: Item };
};
