const {
  CreateClientValidation
} = require('../schema/input/createClientValidation');

const { ErrorHandled } = require('ebased/util/error');

const createClient = require('../service/createClient');

const getAge = require('../helper/getAge');

const { ClientCreatedEvent } = require('../schema/event/clientCreated');
const { publishClientCreated } = require('../service/publishClientCreated');

const getClientByDni = require('../service/getClientsByDni');

module.exports = async (commandPayload, commandMeta) => {
  // Input validation
  new CreateClientValidation(commandPayload, commandMeta);
  const { dni, name, surname, dateOfBirth } = commandPayload;

  const age = getAge(dateOfBirth);

  if (age > 65)
    throw new ErrorHandled('Mayor de edad', {
      status: 404,
      code: 'code0xx',
      layer: 'domain'
    });

  // VALIDAR QUE NO HAYA DUPLICADO ---> GetByID
  const { Item } = await getClientByDni(dni);
  if (Item && Item.dni === dni)
    throw new ErrorHandled('Client already exists', {
      status: 400,
      code: 'code0xx',
      layer: 'domain'
    });

  const clientCreated = {
    dni,
    name,
    surname,
    dateOfBirth,
    status: 'CONFIRMED'
  };

  // Service Async
  await createClient(clientCreated);

  // PUBLICAR EN SNS
  await publishClientCreated(
    new ClientCreatedEvent(clientCreated, commandMeta)
  );

  // Body is always the payload of the command response
  return { body: clientCreated };
};
