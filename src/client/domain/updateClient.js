const {
  UpdateClientValidation
} = require('../schema/input/updateClientValidation');

const { ErrorHandled } = require('ebased/util/error');

const getClientByDni = require('../service/getClientsByDni');

const updateClient = require('../service/updateClient');

const config = require('ebased/util/config');
const createGift = require('../helper/createGift');
const getAge = require('../helper/getAge');
const createCard = require('../helper/createCard');

const TABLE_NAME = config.get('CLIENTS_TABLE');

module.exports = async (commandPayload, commandMeta) => {
  new UpdateClientValidation(commandPayload, commandMeta);
  const { dni, name, surname, dateOfBirth } = commandPayload;

  const { Item } = await getClientByDni(dni);
  if (!Item)
    throw new ErrorHandled('The client does not exist', {
      status: 400,
      code: 'code0xx',
      layer: 'domain'
    });

  if (dateOfBirth !== Item.dateOfBirth) {
    Item.gift = createGift(dateOfBirth);

    const newAge = getAge(dateOfBirth);
    if (
      (Item.creditCard.type === 'Classic' && newAge > 45) ||
      (Item.creditCard.type === 'Gold' && newAge < 45)
    ) {
      Item.creditCard = createCard(newAge);
    }
  }

  const { Attributes } = await updateClient({
    table: TABLE_NAME,
    partitionKey: 'dni',
    partitionKeyValue: Item.dni,
    body: {
      name: name || Item.name,
      surname: surname || Item.surname,
      dateOfBirth: dateOfBirth || Item.dateOfBirth,
      gift: Item.gift,
      creditCard: Item.creditCard
    }
  });

  return { status: 200, body: Attributes };
};
