const getClients = require('../service/getClients');

module.exports = async (commandPayload, commandMeta) => {
  const data = await getClients();

  return { status: 200, body: data };
};
