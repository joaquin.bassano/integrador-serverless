const {
  CreateClientValidation
} = require('../schema/input/createClientValidation');

const getAge = require('../helper/getAge');
const createCard = require('../helper/createCard');

const updateCard = require('../service/updateCard');

module.exports = async (eventPayload, eventMeta) => {
  const body = JSON.parse(eventPayload.Message);

  new CreateClientValidation(body, eventMeta);

  const { dni, dateOfBirth } = body;

  const age = getAge(dateOfBirth);
  const creditCard = createCard(age);

  await updateCard(dni, creditCard);
};
