const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const updateCard = async (dni, creditCard) => {
  const params = {
    TableName: TABLE_NAME,
    Key: { dni },
    ExpressionAttributeNames: {
      '#CC': 'creditCard'
    },
    UpdateExpression: 'set #CC = :cc',
    ExpressionAttributeValues: {
      ':cc': creditCard
    }
  };

  await dynamo.updateItem(params);
};

module.exports = updateCard;
