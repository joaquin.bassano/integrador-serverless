const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const deleteClient = async dni => {
  const params = {
    TableName: TABLE_NAME,
    Key: { dni },
    ExpressionAttributeNames: {
      '#S': 'status'
    },
    UpdateExpression: 'set #S = :s',
    ExpressionAttributeValues: {
      ':s': 'DELETED'
    }
  };

  await dynamo.updateItem(params);
};

module.exports = deleteClient;
