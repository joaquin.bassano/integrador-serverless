const dynamo = require('ebased/service/storage/dynamo');

const updateClient = async query => {
  let primaryKey = { [query.partitionKey]: query.partitionKeyValue };
  let updateExpression = 'set';
  let expressionAttributeNames = {};
  let expressionAttributeValues = {};

  for (const property in query.body) {
    updateExpression += ` #${property} = :${property} ,`;
    expressionAttributeNames['#' + property] = property;
    expressionAttributeValues[':' + property] = query.body[property];
  }

  if (query.sortKey) primaryKey[query.sortKey] = query.sortKeyValue;

  updateExpression = updateExpression.slice(0, -1);

  const params = {
    TableName: query.table,
    Key: primaryKey,
    UpdateExpression: updateExpression,
    ExpressionAttributeNames: expressionAttributeNames,
    ExpressionAttributeValues: expressionAttributeValues,
    ReturnValues: 'ALL_NEW'
  };

  return dynamo.updateItem(params);
};

module.exports = updateClient;
