const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const getClientByDni = async dni => {
  var params = {
    TableName: TABLE_NAME,
    Key: { dni }
  };

  const client = await dynamo.getItem(params);

  if (client.Item !== undefined && client.Item.status === 'DELETED')
    client.Item = undefined;

  return client;
};

module.exports = getClientByDni;
