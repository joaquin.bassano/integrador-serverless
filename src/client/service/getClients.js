const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const getClients = async () => {
  var params = {
    ExpressionAttributeValues: {
      ':s': 'CONFIRMED'
    },
    FilterExpression: '#S = :s',
    ExpressionAttributeNames: {
      '#S': 'status'
    },
    TableName: TABLE_NAME
  };

  const clients = await dynamo.scanTable(params);

  return clients;
};

module.exports = getClients;
