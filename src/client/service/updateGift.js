const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

const updateGift = async (dni, gift) => {
  const params = {
    TableName: TABLE_NAME,
    Key: { dni },
    ExpressionAttributeNames: {
      '#G': 'gift'
    },
    UpdateExpression: 'set #G = :g',
    ExpressionAttributeValues: {
      ':g': gift
    }
  };

  await dynamo.updateItem(params);
};

module.exports = updateGift;
