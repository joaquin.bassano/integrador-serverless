const getRandomInt = require('./getRandomInt');

const createCard = age => {
  const creditCard = {
    type: age < 45 ? 'Classic' : 'Gold',
    number:
      age < 45
        ? getRandomInt(4000000000000000, 4999999999999999)
        : getRandomInt(5000000000000000, 5999999999999999),
    expiration: `${getRandomInt(01, 32)}-${getRandomInt(1, 13)}`,
    cvc: getRandomInt(100, 1000)
  };

  return creditCard;
};

module.exports = createCard;
