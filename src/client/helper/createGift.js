const createGift = dateOfBirth => {
  const dateParsed = new Date(dateOfBirth);
  const currentMonth = dateParsed.getMonth() + 1;

  if (currentMonth === 12 || currentMonth === 1 || currentMonth === 2)
    return 'Remera';
  else if (currentMonth === 3 || currentMonth === 4 || currentMonth === 5)
    return 'Buzo';
  else if (currentMonth === 6 || currentMonth === 7 || currentMonth === 8)
    return 'Sweater';
  else if (currentMonth === 9 || currentMonth === 10 || currentMonth === 11)
    return 'Camisa';
};

module.exports = createGift;
