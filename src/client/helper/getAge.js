const getAge = dateString => {
  let today = new Date();
  let birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  let differenceMonths = today.getMonth() - birthDate.getMonth();
  if (
    differenceMonths < 0 ||
    (differenceMonths === 0 && today.getDate() < birthDate.getDate())
  ) {
    age--;
  }
  return age;
};

module.exports = getAge;
