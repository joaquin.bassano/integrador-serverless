const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CLIENT_CREATED',
      specversion: 'v1.0.0',
      payload: payload,
      meta: meta,
      schema: {
        dni: { type: String, max: 9, required: true },
        name: { type: String, required: true },
        surname: { type: String, required: true },
        dateOfBirth: { type: String, required: true },
        status: { type: String, required: true }
      }
    });
  }
}

module.exports = { ClientCreatedEvent };
