const { InputValidation } = require('ebased/schema/inputValidation');

class GetClientValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.GET_CLIENT',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        dni: { type: String, max: 9, required: true }
      }
    });
  }
}

module.exports = { GetClientValidation };
