const { InputValidation } = require('ebased/schema/inputValidation');

class UpdateClientValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.UPDATE_CLIENT',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        dni: { type: String, max: 9, required: true },
        name: { type: String, required: false },
        surname: { type: String, required: false },
        dateOfBirth: { type: String, required: false }
      }
    });
  }
}

module.exports = { UpdateClientValidation };
