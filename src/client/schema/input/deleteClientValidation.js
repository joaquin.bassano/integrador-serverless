const { InputValidation } = require('ebased/schema/inputValidation');

class DeleteClientValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.DELETE_CLIENT',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        dni: { type: String, max: 9, required: true }
      }
    });
  }
}

module.exports = { DeleteClientValidation };
