const { InputValidation } = require('ebased/schema/inputValidation');

class CreateClientValidation extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'CLIENT.CREATE_CLIENT',
      specversion: 'v1.0.0',
      source: meta.source,
      payload: payload,
      schema: {
        strict: false,
        dni: { type: String, max: 9, required: true },
        name: { type: String, required: true },
        surname: { type: String, required: true },
        dateOfBirth: { type: String, required: true }
      }
    });
  }
}

module.exports = { CreateClientValidation };
